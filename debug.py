#-*- coding:utf-8 -*-
from foo.model import *

item_sim_matrix = np.load('data/item_sim_matrix.npy', allow_pickle=True)
item_list = np.load('data/item_list.npy', allow_pickle=True)
user_dict = np.load('data/user_dict.npy', allow_pickle=True)

if __name__ == '__main__':
    print(user_dict)