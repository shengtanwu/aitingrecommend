from flask import Flask, request
from foo.model import *

item_sim_matrix = np.load('data/item_sim_matrix.npy', allow_pickle=True)
item_list = list(np.load('data/item_list.npy', allow_pickle=True))
user_dict = np.load('data/user_dict.npy', allow_pickle=True).item()

app = Flask(__name__)


@app.route('/predict', methods=['GET'])
def predict():
    """
    :param user_id
    :return: topk感兴趣的书单。
    """
    k = 12
    user_id = request.args.get('user_id', '')
    user_id = int(user_id)
    score = None
    # books_in_score = []
    # if user not in dictionary, return empty list.
    if user_id not in user_dict:
        return {'list': []}
    # 遍历用户已购书单
    for book in user_dict[user_id]:
        # 取书籍在矩阵中的Index
        i = item_list.index(book)
        # 用户对书籍的评分在这里为是否购买，对其他书籍的兴趣度即为书籍i对其他书的相似度向量，遍历i累加相似度向量即最后兴趣度向量
        if score is None:
            score = item_sim_matrix[i].copy() # 初始化score向量
        else:
            score += item_sim_matrix[i]
        score[i] -= 1 # 在第i个分数上扣除这本书对自己的相似度1
    assert k <= score.shape[0]

    #选取topk书单，待优化。
    ranked = np.argsort(score)
    indexes = ranked[::-1][:k]
    topk = []
    for i in indexes:
        topk.append(int(item_list[i]))
        # topk.update({item_list[i]: score[i]})
    return {'list': topk}


if __name__ == '__main__':
    app.run(host='0,0,0,0', debug=True)
