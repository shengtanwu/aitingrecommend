#-*- coding:utf-8 -*-
import numpy as np


def get_user_item_dict(df):
    '''
    :param df:
    :return:
    '''
    item_sim_features = ['UserID', 'BookID']
    df = df[item_sim_features]
    df_tuples = [tuple(xi) for xi in df.values]
    item_dict = {}
    user_dict = {}
    # 将购买记录转化成 书-用户 购买表
    for user_id, book_id in df_tuples:

        if book_id not in item_dict:
            item_dict[book_id] = set([user_id])
        else:
            item_dict[book_id].add(user_id)

        if user_id not in user_dict:
            user_dict[user_id] = set([book_id])
        else:
            user_dict[user_id].add(book_id)

    return item_dict, user_dict


def get_item_similarity_matrix(df):
    '''

    :param df: 用于训练的csv文件
    :return:
    '''

    item_dict, user_dict = get_user_item_dict(df)
    item_list = list(item_dict.keys())
    # 初始化相似度矩阵
    n = len(item_list)
    item_sim_matrix = np.zeros((n, n))

    # 计算相似度矩阵
    for i in range(n):
        for j in range(n):
            user_list_i = item_dict[item_list[i]]
            user_list_j = item_dict[item_list[j]]
            if not user_list_i or not user_list_j:
                item_sim_matrix[i][j] = 0
                continue
            if item_sim_matrix[j][i] != 0:
                item_sim_matrix[i][j] = item_sim_matrix[j][i]
                continue
            num_shared_user = len(user_list_i.intersection(user_list_j))
            item_sim_matrix[i][j] = float(num_shared_user) / np.sqrt(len(user_list_i) * len(user_list_j))
    np.save('../data/item_sim_matrix.npy', item_sim_matrix)
    np.save('../data/item_list.npy', item_list)
    np.save('../data/user_dict.npy', user_dict)
    return item_dict, user_dict, item_list, item_sim_matrix


def _predict(item_sim_matrix,
             item_list,
             user_id,
             user_dict,
             k):
    score = None
    for book in user_dict[user_id]:
        i = item_list.index(book)
        if not score:
            score = item_sim_matrix[i]
        else:
            score += item_sim_matrix[i]
        score[i] -= item_sim_matrix[i][i]

    assert k <= score.shape[0]
    indexes = np.argsort(score)[-k:]
    topk = list(reversed([(item_list[i], score[i]) for i in indexes]))
    # debug不给用户推送已经买了的商品
    # print(user_dict[user_id])
    return topk


def corner_cases():
    '''
    冷启动情况
    :return:
    '''
    pass


if __name__ == '__main__':
    item_sim_matrix = np.load('../data/item_sim_matrix.npy', allow_pickle=True)
    item_list = np.load('../data/item_list.npy', allow_pickle=True)
    user_dict = np.load('../data/user_dict.npy', allow_pickle=True)
    # _predict(item_sim_matrix, item_list, '19000', user_dict, k = 10)
    type = user_dict
